PREDICTING PAROLE VIOLATORS
In many criminal justice systems around the world, inmates deemed not to be a threat 
to society are released from prison under the parole system prior to completing their 
sentence. They are still considered to be serving their sentence while on parole, and 
they can be returned to prison if they violate the terms of their parole.

Parole boards are charged with identifying which inmates are good candidates for 
release on parole. They seek to release inmates who will not commit additional 
crimes after release. In this problem, we will build and validate a model that 
predicts if an inmate will violate the terms of his or her parole. 
Such a model could be useful to a parole board when deciding to approve or 
deny an application for parole.

we will use data from the United States 2004 National Corrections Reporting Program. 

#Load the dataset
Parole <- read.csv("parole.csv")

#Check the structure of the dataset
str(Parole)

#Using the as.factor() function, convert these variables(state, crime) to factors
Parole$state <- as.factor(Parole$state)
Parole$crime <- as.factor(Parole$crime)

#To ensure consistent training/testing set splits
set.seed(144)
library(caTools)
split = sample.split(Parole$violator, SplitRatio = 0.7)
train = subset(Parole, split == TRUE)
test = subset(Parole, split == FALSE)

#Create the model
ParoleLog = glm(violator ~ ., data=train, family=binomial)

#Use the predict() function to obtain the model's predicted probabilities for parolees
# in the testing set.
predictTest <- predict(ParoleLog, type="response", newdata=test)

#What is the maximum predicted probability of a violation?
summary(predictTest)

#evaluate the model's predictions on the test set using a threshold of 0.5.
table(test$violator, predictTest > 0.5)

#What is the model's sensitivity?
# TP/(TP + FN)

#What is the model's specificity?
# TN/(TN + FP)

#What is the accuracy of a simple model that predicts that every parolee is a 
#non-violator?
# (TN + TP)/N   N = number of observation

#Using the ROCR package, what is the AUC value for the model?
library(ROCR)
ROCRpredTest = prediction(predictTest, test$violator)
ROCRpredTest = prediction(predictTest, test$violator)
auc = as.numeric(performance(ROCRpredTest, "auc")@y.values)
auc     
#AUC is the perecentage of time that our model will classify which is which correctly
